﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ePub.Startup))]
namespace ePub
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
