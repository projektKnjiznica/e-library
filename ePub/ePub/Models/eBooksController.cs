﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePub;

namespace ePub.Models
{
    public class eBooksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: eBooks
        public ActionResult Index()
        {
            return View(db.eBooks.ToList());
        }

        // GET: eBooks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eBook eBook = db.eBooks.Find(id);
            if (eBook == null)
            {
                return HttpNotFound();
            }
            return View(eBook);
        }

        // GET: eBooks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: eBooks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "eBookId,Name,CreatorId,CreatedBy,CreationDate,ModifiedBy,ModificationDate")] eBook eBook)
        {
            if (ModelState.IsValid)
            {
                db.eBooks.Add(eBook);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eBook);
        }

        // GET: eBooks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eBook eBook = db.eBooks.Find(id);
            if (eBook == null)
            {
                return HttpNotFound();
            }
            return View(eBook);
        }

        // POST: eBooks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "eBookId,Name,CreatorId,CreatedBy,CreationDate,ModifiedBy,ModificationDate")] eBook eBook)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eBook).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eBook);
        }

        // GET: eBooks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eBook eBook = db.eBooks.Find(id);
            if (eBook == null)
            {
                return HttpNotFound();
            }
            return View(eBook);
        }

        // POST: eBooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            eBook eBook = db.eBooks.Find(id);
            db.eBooks.Remove(eBook);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
