//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ePub
{
    using System;
    using System.Collections.Generic;
    
    public partial class eBook_Series
    {
        public int Id { get; set; }
        public int eBookId { get; set; }
        public int SeriesId { get; set; }
        public int SeriesNumber { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModificationDate { get; set; }
    
        public virtual eBook eBook { get; set; }
        public virtual Series Series { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
    }
}
