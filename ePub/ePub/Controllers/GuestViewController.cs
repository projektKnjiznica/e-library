﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePub;

namespace ePub.Controllers
{
    public class GuestViewController : Controller
    {
        private eLibraryEntities2 db = new eLibraryEntities2();

        // GET: GuestView
        public ActionResult Index()
        {
            var eBooks = db.eBooks.Include(e => e.User).Include(e => e.User1);
            return View(eBooks.ToList());
        }

        // GET: GuestView/Details
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eBook eBook = db.eBooks.Find(id);
            if (eBook == null)
            {
                return HttpNotFound();
            }
            return View(eBook);
        }

        // GET: GuestView/Preview
        public ActionResult Preview(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eBook eBook = db.eBooks.Find(id);
            if (eBook == null)
            {
                return HttpNotFound();
            }
            return View(eBook);
        }

        // GET: GuestView/Export
        public ActionResult Export(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eBook eBook = db.eBooks.Find(id);
            if (eBook == null)
            {
                return HttpNotFound();
            }
            return View(eBook);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
