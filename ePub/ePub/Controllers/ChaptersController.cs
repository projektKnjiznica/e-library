﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePub;
using System.IO;
using System.Text;

namespace ePub.Controllers
{
    public class ChaptersController : Controller
    {
        private eLibraryEntities2 db = new eLibraryEntities2();

        // GET: Chapters
        public ActionResult Index(int? bookId)
        {
            if (bookId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var chapters = db.Chapters.Where(c => c.eBookId == bookId).Include(c => c.eBook).Include(c => c.User).Include(c => c.User1);
            ViewBag.eBookId = bookId;
            ViewBag.eBookName = db.eBooks.Find(bookId).Name;
            return View(chapters.ToList());
        }

        // GET: Chapters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chapter chapter = db.Chapters.Find(id);
            if (chapter == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = db.Chapters.Find(id).eBookId;
            return View(chapter);
        }

        // GET: Chapters/Create
        public ActionResult Create(int bookId)
        {
            int newChapterId;
            if (db.Chapters.Where(c => c.eBookId == bookId).Any())
            {
                newChapterId = db.Chapters.Where(c => c.eBookId == bookId).Max(c => c.ChapterNumber) + 1;
            }
            else
            {
                newChapterId = 1;
            }
            ViewBag.BookId = bookId;
            ViewBag.ChapterNumber = newChapterId;
            return View();
        }

        
        // POST: Chapters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]

        public ActionResult Create(int bookId, [Bind(Include = "html,ChapterNumber")] Chapter chapter)
        {
            //Kako provjeriti je li prazno? tj. kako napraviti da se pojavi "required" error message?
            if (chapter != null && chapter.html == null)
            {
                chapter.html = " ";
            }
            List<int> allChapters = db.Chapters.Where(c => c.eBookId == bookId).Select(c => c.ChapterNumber).ToList();
            if (allChapters.Contains(chapter.ChapterNumber))
            {
                ModelState.AddModelError("", "Chapter already exists!");
            }
            
            
            //zasad user = bilo koji prvi, kasnije to promijeniti
            chapter.CreatedBy = db.Users.First().UserId;
            chapter.ModifiedBy = chapter.CreatedBy;
            chapter.CreationDate = DateTime.Now;
            chapter.ModificationDate = DateTime.Now;
            chapter.eBookId = bookId;

            if (ModelState.IsValid)
            {
                db.Chapters.Add(chapter);
                db.SaveChanges();
                return RedirectToAction("Index", new { bookId = bookId});
            }

            return View(chapter);
        }

        // GET: Chapters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chapter chapter = db.Chapters.Find(id);
            if (chapter == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.content = chapter.html;
            ViewBag.id = chapter.eBookId;
            ViewBag.ChapterNumber = chapter.ChapterNumber;
            ViewBag.ebookName = chapter.eBook.Name;
            return View(chapter);
        }

        // POST: Chapters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ChapterId,ChapterNumber,ebook,ebookId,html,CreatedBy,ModifiedBy,CreationDate,User,User1")] Chapter chapter)
        {
            List<int> allChapters = db.Chapters.Where(c => c.eBookId == chapter.eBookId 
                                                     && c.ChapterId != chapter.ChapterId ).Select(c => c.ChapterNumber).ToList();
            allChapters.Remove(chapter.ChapterId);
            if (allChapters.Contains(chapter.ChapterNumber))
            {
                ModelState.AddModelError("", "Chapter already exists!");
            }
            if (chapter != null && chapter.html == null)
            {
                chapter.html = " ";
            }
            chapter.ModificationDate = DateTime.Now;
            //Promijeniti u User na kraju
            chapter.ModifiedBy = db.Users.First().UserId;
            if (ModelState.IsValid)
            {
                db.Entry(chapter).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { bookId = chapter.eBookId});
            }
            
            return View(chapter);
        }

        // GET: Chapters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chapter chapter = db.Chapters.Find(id);
            if (chapter == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookId = chapter.eBookId;
            return View(chapter);
        }

        // POST: Chapters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Chapter chapter = db.Chapters.Find(id);
            int tempId = chapter.eBookId;
            db.Chapters.Remove(chapter);
            db.SaveChanges();
            return RedirectToAction("Index", new { bookId = tempId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
