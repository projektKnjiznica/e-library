﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePub;

namespace ePub.Controllers
{
    public class eBooksController : Controller
    {
        private eLibraryEntities2 db = new eLibraryEntities2();

        // GET: eBooks
        public ActionResult Index()
        {
            var eBooks = db.eBooks.Include(e => e.User).Include(e => e.User1);
            return View(eBooks.ToList());
        }

        // GET: eBooks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eBook eBook = db.eBooks.Find(id);
            if (eBook == null)
            {
                return HttpNotFound();
            }
            ViewBag.CreatedBy = eBook.User.Username;
            ViewBag.ModifiedBy = eBook.User1.Username;
            return View(eBook);
        }

        // GET: eBooks/Create
        public ActionResult Create()
        {
            //USER!!!
            ViewBag.CreatedBy = db.Users.First().Username;
            return View();
        }

        // POST: eBooks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "eBookId,Name,CreatedBy,CreationDate,ModifiedBy,ModificationDate,User,User1")] eBook eBook)
        {

            //users treba napraviti kako treba!!
            eBook.CreatedBy = db.Users.First().UserId;
            eBook.ModifiedBy = eBook.CreatedBy;
            eBook.CreationDate = DateTime.Now;
            eBook.ModificationDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.eBooks.Add(eBook);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CreatedBy = new SelectList(db.Users, "UserId", "UserId", eBook.CreatedBy);
            ViewBag.ModifiedBy = new SelectList(db.Users, "UserId", "UserId", eBook.ModifiedBy);

            return View(eBook);
        }

        // GET: eBooks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eBook eBook = db.eBooks.Find(id);
            if (eBook == null)
            {
                return HttpNotFound();
            }
            
            return View(eBook);
        }

        // POST: eBooks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "eBookId,Name,CreatedBy,CreationDate,ModifiedBy,ModificationDate,User,User1")] eBook eBook)
        {
            eBook.ModificationDate = DateTime.Now;
            //promijeniti user da uzima pravi
            eBook.ModifiedBy = db.Users.First().UserId;
            if (ModelState.IsValid)
            {
                db.Entry(eBook).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            return View(eBook);
        }

        // GET: eBooks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eBook eBook = db.eBooks.Find(id);
            if (eBook == null)
            {
                return HttpNotFound();
            }
            return View(eBook);
        }

        // POST: eBooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            List<Chapter> chapters = db.Chapters.Where(c => c.eBookId == id).ToList(); 
            foreach(Chapter ch in chapters)
            {
                db.Chapters.Remove(ch);
            }
            eBook eBook = db.eBooks.Find(id);
            db.eBooks.Remove(eBook);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Export(int? id)
        {
            return RedirectToAction("Index");
        }
   
    }
}
